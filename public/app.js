'use strict';
function addUserFetch(userInfo) {
  // напишите POST-запрос используя метод fetch
}

function getUserFetch(id) {
  // напишите GET-запрос используя метод fetch
}

function addUserXHR(userInfo) {
  // напишите POST-запрос используя XMLHttpRequest
}

function getUserXHR(id) {
  // напишите GET-запрос используя XMLHttpRequest
}

function checkWork() {  
  addUserFetch({ name: "Alice", lastname: "FetchAPI" })
    .then(userId => {
      console.log(`Был добавлен пользователь с userId ${userId}`);
      return getUserFetch(userId);
    })
    .then(userInfo => {
      console.log(`Был получен пользователь:`, userInfo);
    })
    .catch(error => {
      console.error(error);
    });
}

// checkWork();